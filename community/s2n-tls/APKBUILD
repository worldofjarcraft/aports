# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer:
pkgname=s2n-tls
pkgver=1.3.51
pkgrel=0
pkgdesc="AWS C99 implementation of the TLS/SSL protocols"
url="https://github.com/aws/s2n-tls"
# s390x: fails a bunch of tests
arch="all !s390x"
license="Apache-2.0"
depends_dev="openssl-dev"
makedepends="
	$depends_dev
	cmake
	linux-headers
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/aws/s2n-tls/archive/refs/tags/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		-DUNSAFE_TREAT_WARNINGS_AS_ERRORS=OFF \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	timeout 600 \
	ctest --test-dir build --output-on-failure \
		-E "(s2n_mem_usage_test|s2n_random_test|s2n_fork_generation_number_test|s2n_connection_test|s2n_self_talk_nonblocking_test)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/s2n
}

sha512sums="
024e44b599f42f8ad653484c4449ba39c56672e1de010be70cf66f71e53246f8732e45525a47d51c5a6cb209bb40db07addb6b96df58ce60209190e9d0ba6ba4  s2n-tls-1.3.51.tar.gz
"
