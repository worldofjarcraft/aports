# Contributor: Martell Malone <martellmalone@gmail.com>
# Maintainer:
pkgname=py3-scipy
pkgver=1.11.2
pkgrel=0
pkgdesc="Python library for scientific computing"
url="https://www.scipy.org/"
arch="all"
license="BSD-3-Clause"
depends="py3-pooch py3-numpy"
makedepends="
	cython
	gfortran
	openblas-dev
	py3-gpep517
	py3-meson-python
	py3-numpy-dev
	py3-numpy-f2py
	py3-pybind11-dev
	py3-wheel
	python3-dev
	pythran
	"
subpackages="$pkgname-tests $pkgname-pyc"
source="https://pypi.io/packages/source/s/scipy/scipy-$pkgver.tar.gz"
builddir="$srcdir"/scipy-$pkgver
options="!check" # TODO

replaces=py-scipy # Backwards compatibility
provides=py-scipy=$pkgver-r$pkgrel # Backwards compatibility

build() {
	export CFLAGS="$CFLAGS -O3 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -O3 -flto=auto"

	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

tests() {
	find "$pkgdir" -name tests -type d | while read -r p; do
		amove "${p#"$pkgdir"}"
	done
}

sha512sums="
f6902d48617827d01f69c057f3c4790c7388bc58df0791ade96e073740253bc0529475f9fc22d00c23c0b649acaeb820792528d3805d0ac68588c329aa87b3f2  scipy-1.11.2.tar.gz
"
