# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=cargo-modules
pkgver=0.9.2
pkgrel=0
pkgdesc="A cargo plugin for showing an overview of a crate's modules"
url="https://github.com/regexident/cargo-modules"
# s390x: FTBFS
arch="all !s390x"
license="MPL2"
makedepends="
	cargo
	cargo-auditable
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/regexident/cargo-modules/archive/$pkgver.tar.gz"
options="net" # needed to fetch crates

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 target/release/cargo-modules -t "$pkgdir"/usr/bin
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
}

sha512sums="
c61a4bbe74c4c9b5d7da8e6d130ea8262d3dd3f0afc03ac86e10bd45cdf24bcf14ed47d713cbedea2d58f8173ff76f8f758e29a1e6bae57851f727fbd22ce609  cargo-modules-0.9.2.tar.gz
"
